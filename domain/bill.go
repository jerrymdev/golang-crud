package domain

import (
	"crud.com/bill/core/database"
	"github.com/google/uuid"
)

type Bill struct {
	ID               uuid.UUID `"json:id"`
	Value            float64   `"json:value"`
	PaymentDateLimit string    `"json:paymentDateLimit"`
	DebtorId         uuid.UUID `"json:debtorId"`
}

func (bill Bill) Create() Bill {
	bill.ID = uuid.New()
	database.Db().Create(&bill)
	return bill
}

func (bill Bill) GetBills(offset int, limit int) []Bill {
	var bills []Bill
	database.Db().Limit(limit).Offset(offset).Find(&bills)
	return bills
}
