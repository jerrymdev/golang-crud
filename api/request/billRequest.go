package request

type BillRequest struct {
	Value            float64 `"json:value"`
	PaymentDateLimit string  `"json:paymentDateLimit"`
	DebtorId         string  `"json:debtorId"`
}
