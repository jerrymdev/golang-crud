package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	request "crud.com/bill/api/request"
	domain "crud.com/bill/domain"
)

func CreateBill(c *gin.Context) {

	var billRequest *request.BillRequest

	if err := c.BindJSON(&billRequest); err != nil {
		return
	}

	//todo utilizar converter
	bill := &domain.Bill{}
	bill.Value = billRequest.Value
	bill.PaymentDateLimit = billRequest.PaymentDateLimit
	bill.DebtorId = uuid.New()

	c.JSON(http.StatusCreated, bill.Create())
}

func GetBills(c *gin.Context) {
	bill := &domain.Bill{}

	c.JSON(http.StatusOK, bill.GetBills(0, 100))
}
