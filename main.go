package main

import (
	"crud.com/bill/api/router"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.POST("/bill", router.CreateBill)

	r.GET("/bills", router.GetBills)

	r.Run()
}
